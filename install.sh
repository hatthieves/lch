#!/bin/bash

if [ "$UID" != "0" ]; then 
  echo " Only root can execute this script, sorry."
  echo " Try 'sudo ./install.sh'"
  exit 0
fi

echo ""
echo "======================"
echo "    Installing lch"
echo "======================"
echo ""
cp lch /usr/bin/lch
chmod 755 /usr/bin/lch
echo " Copied lch in /usr/bin"

if [ -z "$MANPATH" ];then
  MANPATH=$(manpath)
fi

MANDIR=${MANPATH%%:*}/man1 
mkdir -p $MANDIR
cp lch.1 $MANDIR/lch.1
gzip $MANDIR/lch.1
chmod 644 $MANDIR/lch.1.gz
echo " Copied manpage in $MANDIR"
echo ""
echo "============================="
echo " lch installed successfully."
echo "============================="
echo ""
echo " Run 'lch --help' or 'man lch' to see options."
exit 0

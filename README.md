#Linux compressor humanizer

###### Compress
```
$ lch file.zip archive1/ [archive2/] [...]
```

###### Decompress
```
$ lch file.zip 
```

## Supported extensions
- tar            
- tar.gz 
- tar.bz2
- tar.xz
- bz2 
- tgz
- gz
- zip
- 7z

## Install
```
sudo ./install.sh
```
